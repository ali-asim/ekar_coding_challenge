import Vue from 'vue'
import axios from 'axios'
import moxios from 'moxios'
import Product from '@/components/Product'

describe('Product.vue', () => {
  beforeEach(function () {
    moxios.install()
  })
  afterEach(function () {
    moxios.uninstall()
  })
  it('should render the api request object from lcbo for a specific product', () => {
    moxios.stubRequest(/product\/.+/, {
      status: 200,
      responseText: 'hello'
    })
  })
})
