import Vue from 'vue'
import axios from 'axios'
import moxios from 'moxios'
import Products from '@/components/Products'

describe('Products.vue', () => {
  beforeEach(function () {
    moxios.install()
  })
  afterEach(function () {
    moxios.uninstall()
  })
  it('should render the api request object from lcbo', () => {
    moxios.stubRequest('/', {
      status: 200,
      responseText: 'hello'
    })
  })
})