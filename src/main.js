// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false

/* eslint-disable no-new */
Vue.mixin({
  data () {
    return {
      get api_url () {
        return 'http://lcboapi.com/'
      },
      get access_key () {
        return 'MDpmNDAyYmQ3ZS1lNmNhLTExZTctYmE5NS1mZjVkMmQzZWQ0MmQ6d1VuRnlnNTdmR3lyZ3NUSDUxbExObEFMMUxkb1FaSElkMDY4'
      }
    }
  }
})
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
