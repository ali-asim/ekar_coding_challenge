# README #
This is a quick api request app for demonstration purposes only :)

### What is under the hood? ###
* vue js
* axios
* scss

### API & Brand ###
* [LCBO API](https://lcboapi.com/)
* Brand guidelines is inherited from - [BEAU'S](https://beaus.ca/)

### But what about the tools? ###
* visual studio code
* vue cli
* webpack
* npm
* git

### How do I get set up? ###
* git clone
* npm install
* npm start